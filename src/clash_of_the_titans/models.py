from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
	points = models.BigIntegerField(verbose_name='Количество очков', default=0)
	password = models.CharField(verbose_name='Пароль', max_length=2048, blank=True, default='')
