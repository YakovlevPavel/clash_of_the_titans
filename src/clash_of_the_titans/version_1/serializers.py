from rest_framework import serializers

from clash_of_the_titans.models import User


class RegistrationPlayerSerializer(serializers.Serializer):
	username = serializers.CharField(min_length=3, max_length=20)
	email = serializers.EmailField(min_length=3)


# class AuthorizationPlayerSerializer(serializers.Serializer):
# 	username = serializers.CharField(min_length=3, max_length=20)
# 	password = serializers.CharField(min_length=4, max_length=20)


class CountPointsSerializer(serializers.Serializer):
	count = serializers.IntegerField(min_value=1)


class PlayerSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = 'id', 'username', 'points'
