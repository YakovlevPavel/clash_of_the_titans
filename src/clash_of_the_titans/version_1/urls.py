from django.urls import path

from . import views

app_name = 'version1'
# version 1
urlpatterns = [
	path('registration', views.registration),
	# path('authorization', views.authorization),
	path('<int:id_player>', views.accounts),
	path('result_table/<int:id_player>', views.result_table),
]
