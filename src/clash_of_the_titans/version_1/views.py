from django.db import transaction

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from itertools import chain

from clash_of_the_titans.utils import errors

from clash_of_the_titans.models import User
from .serializers import RegistrationPlayerSerializer, PlayerSerializer, CountPointsSerializer


@api_view(['POST'])
@permission_classes(())
def registration(request):
	"""
	post: Регистрация игрока
	---

	## Параметры запроса

	* **username** - **_str_**: Имя/логин игрока
	* **email** - **_str_**: Емейл игрока

	#### Ответ:
		{
			"id": 13,
			"points": 0,
			"username": "palyer_6"
		}

	## Данные ответа

	* **id** - **_int_**: Уникальный идентификатор игрока в системе
	* **points** - **_int_**: Количество очков
	* **username** - **_str_**: Имя/логин игрока
	"""
	data_reg = RegistrationPlayerSerializer(data=request.data)

	if data_reg.is_valid():
		user_name = data_reg.data['username']
		with transaction.atomic():
			player = User.objects.filter(username=user_name)

			if player.exists():
				return Response({'error': errors.PLAYER_ALREADY_EXISTS}, status.HTTP_400_BAD_REQUEST)

			player = User.objects.create(
				username=user_name,
				email=data_reg.data['email'],
			)

		return Response(PlayerSerializer(player).data, status.HTTP_201_CREATED)

	return Response({'error': data_reg.errors}, status.HTTP_400_BAD_REQUEST)


# @api_view(['POST'])
# @permission_classes(())
# def authorization(request):
# 	"""
# 	post: Авторизация игрока
# 	---
#
# 	## Параметры запроса
#
# 	* **username** - **_str_**: Имя/логин игрока
# 	* **password** - **_str_**: Пароль
#
# 	#### Ответ:
#
# 	Такой же как и в методе "Регистрация игрока"
# 	"""
#
# 	data_auth = AuthorizationPlayerSerializer(data=request.data)
#
# 	if data_auth.is_valid():
# 		user_name = data_auth.data['username']
# 		player = User.objects.filter(username=user_name)
#
# 		if not player.exists():
# 			return Response({'error': errors.PLAYER_NOT_FOUND}, status.HTTP_400_BAD_REQUEST)
#
# 		player = player.get()
#
# 		if not check_password(data_auth.data['password'], player.password):
# 			return Response({'error': errors.INVALID_PASSWORD_LOGIN}, status.HTTP_400_BAD_REQUEST)
#
# 		return Response(PlayerSerializer(player).data, status.HTTP_200_OK)
#
# 	return Response({'error': data_auth.errors}, status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PATCH'])
@permission_classes(())
def accounts(request, id_player):
	"""
	get: Получение информации о игроке
	---

	## Параметры запроса
	В url необходимо передать id игрока

	#### Ответ:

	Такой же как и в методе "Регистрация игрока"

	patch: Увеличение количества очков
	___

	## Параметры запроса
	В url необходимо передать id игрока

	* **count** - **_int_**: Число, на которое необходимо увеличить очки игрока

	#### Ответ:

	Такой же как и в методе "Регистрация игрока"
	---
	"""

	player = User.objects.filter(id=id_player)

	if not player.exists():
		return Response({'error': errors.PLAYER_NOT_FOUND}, status.HTTP_404_NOT_FOUND)

	player = player.get()

	if request.method == 'PATCH':
		count_points = CountPointsSerializer(data=request.data)

		if not count_points.is_valid():
			return Response({'error': count_points.errors}, status.HTTP_400_BAD_REQUEST)

		player.points = count_points.data['count']
		player.save()

	return Response(PlayerSerializer(player).data, status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes(())
def result_table(request, id_player):
	"""
	get: Получение результатов турнирной таблицы
	---

	## Параметры запроса
	В url необходимо передать id игрока

	#### Ответ:
		{
			"result_table": [
				{
					"id": 19,
					"points": 40000000000,
					"username": "player_7"
				},
				{
					"id": 17,
					"points": 5000000,
					"username": "player_4"
				},
				{
					"id": 16,
					"points": 100000,
					"username": "player_3"
				}
			]
		}


	## Данные ответа

	* **result_table** - **_obj_**: Список объектов игроков в убывающем порядке по количеству очков + данные игрока чей
	id был передан в url. Данные объекта игрока описаны в методе "Регистрация игрока".
	"""

	players = User.objects.filter(is_staff=False, is_superuser=False).order_by('-points')[:5]
	players = PlayerSerializer(players, many=True).data
	return Response({'result_table': players}, status.HTTP_200_OK)
