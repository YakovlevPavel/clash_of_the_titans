from django.apps import AppConfig


class V1Config(AppConfig):
    name = 'version_1'
