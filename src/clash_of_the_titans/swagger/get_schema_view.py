from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.permissions import IsAdminUser
from rest_framework.schemas import get_schema_view
description = '<h1>В описании каждого метода не обращать внимание на блок Response Messages!!!</h1>' \
'<h2>Адрес для отправки запросов - http://51.144.115.56/</h2>'


schema_view = get_schema_view(
	title='Документация API',
	renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer],
	permission_classes=[IsAdminUser],
	description=description
)
