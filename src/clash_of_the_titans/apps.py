from django.apps import AppConfig


class ClashOfTheTitans(AppConfig):
	name = 'clash_of_the_titans'
	label = 'clash_of_the_titans'
